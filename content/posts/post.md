---
title: 線段重疊<大測資>
date: 2022-04-24 21:12:52
tags:
---
```bash
#n=5
#lines=[[160,180],[150,200],[280,300],[300,330],[190,210]]
n=int(input())
lines=[list(map(int,input().split())) for i in range(n)]
def merge(lineA,lineB):#return OVERLAP
    overlap=False
    #b頭a尾有重疊
    if lineB[0]<=lineA[1]:
        if lineB[1]>lineA[1]:
            lineA[1]=lineB[1]#已經改變lineA的值，不用再回傳
        return True #overlap=True
    else:
        return False
lines.sort()
#print("sort start=",lines)
current=[0,0]
total=0
for line in lines:
    if merge(current,line):
        pass
    else:
        total+=(current[1]-current[0])#listA不和listB重疊，計算listA的總長
        current=line
    #print("total=",total,"current=",current)
total+=(current[1]-current[0])#最後那段還沒被加進去!
print(total)
```
{{<raw>}}<br>
<div>
    <hr>
    <p style="margin-bottom: 0;">如果覺得這篇文章有幫助的話，可以免費點讚贊助我!( •̀ ω •́ )✧</p>
    <iframe width="100%" height="230px" scrolling="no" frameborder="0" id="player" src="https://button.like.co/in/embed/nataliepjlin/button/?referrer=<%- post.permalink %>" allowfullscreen="true"></iframe>
</div>
{{</raw>}}