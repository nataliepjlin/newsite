---
title: "c296: APCS-2016-1029-3定時K彈"
date: 2022-04-23 16:48:56
categories: [APCS]
tags: [c296,Python]
---
# Code
## 小測資
```bash
n,m,k=5,2,4#共n人，炸掉開始後的第m人，爆k次
#輸入list1,移除炸掉的
list1=list(range(1,n+1))
#print(list1)
def kkbomb(list1,m,k):
    for i in range(k):
        for j in range(m-1):
            list1.append(list1.pop(0)) #把炸掉的元素「前面的」元素推回list1。pop(0)才會移除最前面的，pop()是移除頂層元素
        list1.pop(0)
        #print("now=",list1)
    return list1
list1=kkbomb(list1,m,k)
print(list1[0])
```
## 大測資
```bash
#n,m,k=8,3,6#共n人，炸掉開始後的第m人，爆k次
def findlucky(luckyidx,list1,m):
    print("before=",list1)
    popidx=(luckyidx+m-1)%len(list1)
    list1.pop(popidx)
    luckyidx=(popidx+len(list1))%len(list1)
    print("nowlist1=",list1)
    return luckyidx
n,m,k=map(int,input().split())
list1=list(range(1,n+1))
luckyidx=0
for _ in range(k):
    luckyidx=findlucky(luckyidx,list1,m)
    print("luckyidx=",luckyidx,"luckyitem=",list1[luckyidx])
print(list1[luckyidx])#不能print(list1)，不一定有炸到剩一個
```
{{<raw>}}<br>
<div>
    <hr>
    <p style="margin-bottom: 0;">如果覺得這篇文章有幫助的話，可以免費點讚贊助我!( •̀ ω •́ )✧</p>
    <iframe width="100%" height="230px" scrolling="no" frameborder="0" id="player" src="https://button.like.co/in/embed/nataliepjlin/button/?referrer=<%- post.permalink %>" allowfullscreen="true"></iframe>
</div>
{{</raw>}}