---
title: "Matrix"
subtitle: ""
date: 2022-06-26T11:18:26+08:00
lastmod: 2022-06-26T11:18:26+08:00
draft: false
description: ""
license: ""
images: []

tags: []
categories: [Me Typing Codes]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
math:
  enable: true
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---


## 題目
其實這是數學題喇，但當時沒注意到他只有10x10，不存在(11,1)這個位置然後百思不得其解，想說用程式輸出觀察看看。才發現，要找60的位置的時候就必須從56(第10列第2行)開始斜向上找。

題目長這樣
$$
  A=[ a_{ij} ]_{10\times10}= \left[\begin{array}{rrrr}
    1 & 3 & 6 & 10 & ... \\\\
    2 & 5 & 9 & ... & ...\\\\
    4 & 8 & .. & ..& ...\\\\
    .. & ..& 95 & 98 & 100\\\\
  \end{array}\right]
$$

## Python Code

```Python
list1=[[0]*10 for _ in range(10)]
for i in range(10):
    list1[0][i]=sum(range(1,i+2))
print(list1[0])
for i in range(10):
    row=0
    col=i
    prev=list1[row][col]
    while row<9 and col>0:
        row+=1
        col-=1
        list1[row][col]=prev-1
        prev-=1

for i in range(10):
    list1[9][9-i]=100-sum(range(1,i+2))+1

for i in range(9,0,-1):
    row=9
    col=i
    prev=list1[row][col]
    while row>0 and col<9:
        row-=1
        col+=1
        list1[row][col]=prev+1
        prev+=1

for i in range(10):
    for j in range(10):
        print(list1[i][j],end=" ")
    print("\n")
```

## Output
![](https://i.imgur.com/fBJMhqF.png)

{{<raw>}}<br>
<div>
    <hr>
    <p style="margin-bottom: 0;">如果覺得這篇文章有幫助的話，可以免費點讚贊助我!( •̀ ω •́ )✧</p>
    <iframe width="100%" height="230px" scrolling="no" frameborder="0" id="player" src="https://button.like.co/in/embed/nataliepjlin/button/?referrer=<%- post.permalink %>" allowfullscreen="true"></iframe>
</div>
{{</raw>}}

